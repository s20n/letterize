= letterize(1) =
:doctype: manpage
// SPDX-License-Identifier: BSD-2-Clause
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>

== NAME ==
letterize -  phone-number to letter-mnemonic generator</refpurpose>

== SYNOPSIS ==
*letterize* [word...]

[[description]]
== DESCRIPTION ==
This program tries to help you find a letter mnemonic matching a
given phone number.

It emits to standard output each possible pronounceable mnemonic, one
per line, using the American standard mapping of dial letters to
numbers (2 goes to ABC, 3 to DEF, 4 to GHI, 5 to JKL, 6 to MNO, 7 to
PRS, 8 to TUV, 9 to XYZ).

The program uses a table of pronounceable letter-triples derived from
a dictionary scan.  Each potential mnemonic must be such that all of
its letter-triples are in the table to be emitted.  About 30% of
possible triples are considered pronounceable.

A typical 7-digit phone number has 19,683 possible mnemonics, but this
test usually cuts the list down to a few hundred or so, a reasonable
number to eyeball-check.  For some numbers, the list will, sadly, be
empty.

It's best to leave out punctuation such as dashes and parens.

[[bugs]]
== BUGS ==
The filtering method doesn't know what plausible medial triples are
not reasonable at the beginnings and ends of words.

I'm not sure what table position 0 (which is what 0 and 1 are mapped
to) means.  If you figure it out, you tell me.  I really should have
generated my own table, but that would have been more work than this
seemed worth -- if your number contains either, you probably need
to generate your mnemonic in disjoint pieces around the digits anyway.

[[author]]
== AUTHOR ==
Eric S. Raymond <esr@snark.thyrsus.com>.  It's based on a table of
plausible letter-triples that had no name attached to it.  Surf to
<ulink http://www.catb.org/~esr/ for updates and related resources.

// end
